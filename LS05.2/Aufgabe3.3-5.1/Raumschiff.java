	/**
	 * Das Raumschiff kann Phaserkanonen abzuschießen. 
	 * Die Methode bekommt ein Raumschiff übergeben, dass das Ziel der Phaserkanone ist.
	 * Falls die Energieversorgung im Raumschiff unter 50 Prozent liegt,
	 * dann wird die Methode nachrichtAnAlle() mit der Nachricht "-=*Click*=-" aufgerufen.
	 * Andernfalls wird die Energieversorgung des Raumschiffs um 50 Prozent reduziert,
	 * die eigene Methode nachrichtAnAlle() mit der Nachricht "Phaserkanone abgeschossen" als Argument aufgerufen und
	 * die eigene Methode treffer() mit dem übergebenen Raumschiff als Argument aufgerufen.
	 * @param r  Raumschiff das mit den Phaserkanonen abgeschossen wird.
	 */
	public void phaserkanoneSchiessen(Raumschiff r){
		if (energieversorgungInProzent < 50)
			nachrichtAnAlle("-=*Click*=-");
		else
		{
			energieversorgungInProzent -= 50;
			nachrichtAnAlle("Phaserkanone abgeschossen");
			treffer(r);
		}
	}

	/**
	 * Ein Raumschiff kann von verschiedenen Waffen getroffen werden. 
	 * Dabei wird eine nicht öffentliche Methode aufgerufen, 
	 * die vermerkt, ob das Raumschiff getroffen wird und 
	 * das Ausmaß des Treffers berechnet. 
	 * Die Schilde des getroffenen Raumschiffs werden um 50% geschwächt.
	 * Sollte anschließend die Schilde vollständig zerstört worden sein, 
	 * in dem der Zustand der Schilde in Prozent unter Null liegt,
	 * so wird der Zustand der Hülle und der Energieversorgung jeweils um 50% abgebaut.
	 * Sollte danach der Zustand der Hülle auf oder unter 0% absinken, 
	 * so sind die Lebenserhaltungssysteme vollständig zerstört und werden auf 0 gesetzt.
	 * Die eigene Methode nachrichtAnAlle() wird mit der Nachricht 
	 * "Alle Lebenserhaltssysteme abgeschaltet." als Argument aufgerufen.
	 * @param r  Raumschiff das getroffen wurde
	 */
	private void treffer(Raumschiff r) {
		r.schildeInProzent -= 50;
		if (r.schildeInProzent <= 0)
		{
			r.huelleInProzent -= 50;
			r.energieversorgungInProzent -= 50;
		}

		if (r.huelleInProzent <= 0)
		{
			r.lebenserhaltungssystemeInProzent = 0;
			r.nachrichtAnAlle("Alle Lebenserhaltssysteme abgeschaltet.");
		}
	}
	
	/**
	 * Es können von einem Raumschiff Nachrichten an Alle gesendet werden. 
	 * Dafür muss die Nachricht selbst an die Methode übergeben werden. 
	 * Diese Methode ist von überall aufrufbar.
	 * Effekt.: Die übergebene Nachricht wird der ArrayListe broadcastKommunikator hinzugefügt
	 * @param message  Nachricht, die an Alle gesendet wird.
	 */
	public void nachrichtAnAlle(String message){
		broadcastKommunikator.add(this.schiffsname + ": " + message);
	}

	/**
	 * Die Klasse Raumschiff hat eine öffentliche, 
	 * statische Methode, die alle Einträge des Logbuchs 
	 * aller Raumschiffe in Form einer Liste zurückgibt. 	
	 * @return broadcastKommunikator  der broadcastKommunikator 
	 * (ArrayList mit alle Nachrichten aller Raumschiffe) wird zurückgegeben.
	 */
	public static ArrayList<String> eintraegeLogbuchZurueckgeben(){
		return broadcastKommunikator; 
	}
	
	/**
	 * Sollte als Ladung “Photonentorpedos” vorhanden sein, 
	 * so können diese in die Torpedorohre des Raumschiffs geladen werden. 
	 * Die öffentliche Methode bekommt die Anzahl der Torpedos als Parameter übergeben.
	 * Gibt es keine Ladung “Photonentorpedo” auf dem Schiff, 
	 * wird als Nachricht "Keine Photonentorpedos gefunden!" in der Konsole ausgegeben 
	 * und die Nachricht an alle “-=*Click*=-” ausgegeben.
	 * Ist die Anzahl der einzusetzenden Photonentorpedos größer als 
	 * die Menge der tatsächlich Vorhandenen, so werden alle vorhandenen Photonentorpedos eingesetzt. 
	 * Ansonsten wird die Ladungsmenge “Photonentorpedos” über die Setter Methode vermindert 
	 * und die Anzahl der Photonentorpedo im Raumschiff erhöht.
	 * Konnten Photonentorpedos eingesetzt werden, 
	 * so wird die Meldung “[X] Photonentorpedos eingesetzt” auf der Konsole ausgegeben.
	 * @param anzahlTorpedos  Anzahl der Torpedos, die in die Torpedorohre des Raumschiffs geladen werden sollen.
	 */
	public void photonentorpedosEinsetzen(int anzahlTorpedos) {
		String ladungsbezeichnung = " "; 
		int menge = 0;
		boolean PhotonentorpedosGefunden = false;
		for(Ladung temp : ladungsverzeichnis) {
			ladungsbezeichnung = temp.getBezeichnung();
			menge = temp.getMenge();
			if(ladungsbezeichnung.equals("Photonentorpedos")){
		        PhotonentorpedosGefunden = true;
		        if(anzahlTorpedos >= menge) {
		          System.out.println(menge + "Photonentorpedo(s) eingesetzt.");
		          this.setPhotonentorpedoAnzahl(this.getPhotonentorpedoAnzahl() + menge);
		          temp.setMenge(0); 
		        }
		        else {  
		          System.out.println(anzahlTorpedos + "Photonentorpedo(s) eingesetzt.");
		          this.setPhotonentorpedoAnzahl(this.getPhotonentorpedoAnzahl() + anzahlTorpedos);
		          temp.setMenge(menge - anzahlTorpedos); 
		        }
		      } 
		    }
		    if(!PhotonentorpedosGefunden){
		        System.out.println("Keine Photonentorpedos gefunden!");
		        nachrichtAnAlle("-=*Click*=-");         
		    }
	}